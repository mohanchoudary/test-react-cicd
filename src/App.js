import React from 'react';
//import logo from './logo.svg';
import './App.css';
// import Greet from './components/Greet';
// import Welcome from './components/Welcome'

// import Message from './components/Message'
// import Counter from './components/Counter'

import Hello from './components/Hello'


function App() {
  return (
    <div className="App">
      {/* <Counter/>
      <Message/>
     <Greet name="Bruice" heroName="Batman">
       <p>This is children props.</p>
     </Greet>
     <Greet name="Donald" heroName="Super Man">
       <button>Action</button>
     </Greet>

     <Welcome name="Bruice" heroName="Batman"/>
     <Welcome name="Donald" heroName="Super Man"/> */}
     <Hello />
    </div>
  );
}

export default App;
